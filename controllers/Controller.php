<?php
/**
 * This is a base class of all controllers 
 * with some middleware actions.
 * @author Janne Tyni
 */

require_once('libs/Session.php'); 
require_once('models/User.php');

abstract class controller
{
	private $loggedIn = false;
	
	public function __construct()
	{
		$this->loggedIn = false;
	}
	
	public function __call($method, $arguments)
    {
		if (Session::getView() != 'notfound')
			Session::setView('notfound');
		else
			Session::setView('welcome');
    }
	
	private function authenticate()
	{
		if (Session::isLoggedIn())
		{
			$this->loggedIn = true;
			return true;
		}
		$this->loggedIn = false;
		Session::setView('welcome');
		return false;
	}
	
	public function isLoggedIn()
	{
		return $this->loggedIn;
	}
	
	public function tryLogin($u, $p)
	{
		$id = User::tryLogIn($u, $p);
		if ($id)
			Session::logIn($id);
		return $id;
	}
	
	public function changeView($view) 
	{
		if(!$this->authenticate() && $view != 'persons' ) return 'welcome';
		
		if ($view == 'logout')
		{
			Session::clear();
			$view = 'welcome';
		}
		elseif ($view == 'welcome') // redirect to home page when logged in
			$view = 'home';

		Session::setView($view);
		return $view;
	}
}
