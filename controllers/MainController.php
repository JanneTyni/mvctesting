<?php
/*
	Controller for action requests
*/
require_once('controllers/Controller.php');

require_once('models/User.php');
require_once('models/Person.php');

class MainController extends Controller
{
	private $view;
	
	public function __construct($view)
	{
		$this->view = $view;
	}
	
	public function login()
	{
		$p = $_POST['password'];
		$p = mb_convert_encoding($p, "UTF-8");
		$u = $_POST['username'];
		$p = md5($p);
		$id = $this->tryLogin($u, $p);
		$v = $this->changeView(($id ? 'home' : 'welcome'));
		$this->view->view($v);
	}
	
	public function logout() 
	{
		$v = $this->changeView('logout');
		$this->view->view($v);
	}
	
	public function home()
	{
		$v = $this->changeView('home');
		$this->view->view($v);
	}
	
	public function welcome()
	{
		$v = $this->changeView('welcome');
		$this->view->view($v);
	}
	
	public function persons()
	{
		$v = $this->changeView('persons');
		$persons = Person::getPersons();
		$this->view->with('persons', $persons);
		if ($this->isLoggedIn()) 
		{
			$users =  User::getUsers();
			$orderedUsers = Array();
			/* order users array using persons array */
			foreach($persons as $p)
			{
				$id = $p['id'];
				foreach($users as $u)
				{
					if($id == $u['id']) 
					{
						$orderedUsers[] = $u;
						break;
					}
				}
			}
			$this->view->with('users', $orderedUsers);
			if (User::isAdmin()) 
			{
				$this->view->with('isAdmin', 1);
			}
		}
		
		$this->view->view($v);
	}
	
	public function saveUsers()
	{
		$users = json_decode($_POST['users']);
		
		foreach($users as $u)
		{
			$id = (isset($u->user) ? $u->user : '-1');
			$user = User::find($id);
			$id = (isset($u->id) ? $u->id : '-1');
			$person = Person::find($id);
			
			if(!isset($user)) $user = new User();
			if(!isset($person)) $person = new Person();
			
			if (isset($u->user)) $user->setField('user', $u->user);
			if (isset($u->pwd)) $user->setField('pwd', md5($u->pwd));
			if (isset($u->pri)) $user->setField('pri', $u->pri);
			if (isset($u->id)) $user->setField('id', $u->id);
			if (isset($u->description)) $user->setField('description', $u->description);
			
			if (isset($u->fname)) $person->setField('fname', $u->fname);
			if (isset($u->sname)) $person->setField('sname', $u->sname);
			if (isset($u->sname)) $person->setField('id', $u->id);
			
			$person->save();
			$user->save();
		}
		
		header('Location: ?action=persons');  
	}
	
	public function removeUser()
	{
		$user = User::find($_POST['user']);
		if(!isset($user)) header('Location: ?action=persons');
		$person = Person::find($user->getField('id'));
		$person->remove();
		$user->remove();
		header('Location: ?action=persons'); 
	}
}