<?php
/**
	Model for servside2017_users table
 */
require_once('models/Model.php');
require_once('libs/Session.php');

class User extends Model
{
	protected $table = 'servside2017_users';
	protected $fields = array(
							'id',
							'user',
							'pri',
							'description',
							'pwd'
						);
	protected $id_field = 'user';
	private $user;
	
	public static function tryLogIn($username, $password)
	{
		$user = self::where('user', $username)->where('pwd', $password)->get();
		if (!$user) return false;
		return $user[0]['user'];
	}
	
	public static function getUsers()
	{
		return self::all();
	}
	
	public static function getLoggedUser()
	{
		$id = Session::isLoggedIn(); // returns user id or null
		$user = array('pri' => -1);
	
		if ($id) $user = self::getById($id);
		
		return $user;
	}
	
	public static function isAdmin()
	{
		return (self::getLoggedUser()['pri'] == 1);
	}
}