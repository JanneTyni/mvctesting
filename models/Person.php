<?php
/**
	Model for servside2017_users table
 */
require_once('models/Model.php');
require_once('libs/Session.php');

class Person extends Model
{
	protected $table = 'servside2017_persons';
	protected $fields = array('id', 'fname', 'sname');

	public static function getPersons()
	{
		return self::all();
	}
	
}