<?php
/**
 * Class for session handling
 *
 * @author tynijann 1302064
 */
 
class Session 
{
	static private $vars;
	
	public static function initialize()
    {
		// start session if not already started
		if( !isset($_SESSION) ) {
			session_start();
			self::setView('welcome');
		}
		self::$vars = &$_SESSION; // session variables referenced to vars
		if(!self::varIsset('view')) self::setView('welcome');
    }
	
	public static function getVar($key) 
	{
		return (self::varIsset($key) ? self::$vars[$key] : null);
	}
	
	public static function setVar($key, $value)
	{
		self::$vars[$key] = $value;
	}
	
	public static function varIsset($key)
	{
		return isset(self::$vars[$key]);
	}
	
	public static function setView($view) 
	{
		self::setVar('view', $view);
	}
	
	public static function getView() 
	{
		return self::getVar('view');
	}
	
	public static function logIn($id)
	{
		self::setVar('user_id', $id);
	}
	
	public static function isLoggedIn()
	{
		return (self::getVar('user_id'));
	}
	
	public static function clear() 
	{
		// remove all session variables
		session_unset(); 

		// destroy the session 
		session_destroy(); 
	}
}