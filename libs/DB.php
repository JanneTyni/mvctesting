<?php

/**
 * Database class with querybuilder for models and some ORM features.
 *
 * @author Janne Tyni
 */
 
class DB extends PDO
{
	
	protected $table;
	protected $fields;
	protected $id_field = 'id';
	private $_fields = array();
	private $query;
	private $is_new;
	private static $_i; // temp instance
	
    public function __construct($is_new=true)
    {
        try
        {
            include 'config/database.php';
            parent::__construct( "mysql:host=$servername;dbname=$dbname",
                    $username, $password,
                    array(PDO::ATTR_EMULATE_PREPARES => false,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, 
						PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );

        }
        catch (PDOException $ex)
        {
            echo $ex->getMessage();
        }
		$this->is_new = $is_new;
		$this->query = 'select * from '.$this->table;
    }
	
	private function fieldExists($f)
	{
		foreach($this->fields as $field) 
		{
			if ($field == $f) return true;
		}
		return false;
	}
	
	private function add($s)
	{
		// if query is empty call all function
		if($this->query == '') $this->all();
		$this->query = $this->query.' '.$s;
	}
	
	// add commas to string (foo -> 'foo')
	private function commas($s)
	{
		return '\''.$s.'\'';
	}
	
	public static function getById($id)
	{
		return self::where((new static())->id_field, $id)->get()[0];
	}
	
	public static function find($id)
	{
		$data = self::getById($id); // get data by id
		if (!$data) return null;
		
		$t = new static(false); // new instance
		
		foreach ($data as $key => $value) // assing data to instance fields
		{
			$t->setField($key, $value);
		}
		
		return $t;
	}
	
	public static function all()
	{
		if (!isset(self::$_i)) self::$_i = new static();
		return self::$_i->get();
	}
	
	public static function where($column, $operator = '=', $value = null) 
	{
		if (!isset(self::$_i)) self::$_i = new static();
		// convert operator and value parameters in value
		if ($operator == '=' && $value == null) return; // not enough parameters given
		elseif ($value == null) $value = '= '.self::$_i->commas($operator);
		else $value = $operator." ".self::$_i->commas($value);
		
		$s = 'where';
		if (strpos(self::$_i->query, 'where') !== false) $s = 'and';
		
		// update query
		self::$_i->add($s);
		self::$_i->add($column);
		self::$_i->add($value);
		return self::$_i;
	}
	
    public function get()
	{
		$this->add(';');
		echo($this->query.'<br>');
		$sth = $this->prepare($this->query);
        $sth->execute();
		$result = null;
		if($sth->rowCount() > 0) $result = $sth->fetchAll();
		self::$_i = null;
		return $result;
	}
	
	public function setField($field, $value)
	{
		if (!isset($field) || !isset($value) || !$this->fieldExists($field)) return;
		$this->_fields[$field] = $value;
	}
	
	public function save()
	{
		$query;
		if ($this->is_new)
		{
			$query = 'insert INTO '.$this->table.' ';
			$columns = '(';
			$values = 'VALUES (';

			foreach($this->_fields as $field => $value)
			{
				if ($value == null || $field == null) continue;
				$columns .= $field.', ';
				$values .= $this->commas($value).', ';
			}
			$columns = substr($columns, 0, -2);
			$values = substr($values, 0, -2);
			$columns .= ') ';
			$values .= ');';
			$query .= $columns.$values;
		}
		else 
		{
			$query = 'UPDATE '.$this->table.' SET ';

			foreach($this->_fields as $field => $value)
			{
				if ($value == null || $field == null) continue;
				$query .= $field.' = '.$this->commas($value).', ';
			}
			
			$query = substr($query, 0, -2);
			$query .= ' where '.$this->id_field.' = '.$this->commas($this->_fields[$this->id_field]).';';
		}
		echo($query.'<br>');
		$sth = $this->prepare($query);
		$sth->execute();
		$this->is_new = false;
	}
	
	public function getField($f)
	{
		return (isset($this->_fields[$f]) ? $this->_fields[$f] : null);
	}
	
	public function getPrimaryField()
	{
		return $this->getField($this->id_field);
	}
	
	public function remove()
	{
		$query = 'DELETE FROM '.$this->table.' WHERE '.$this->id_field.'='.$this->commas($this->getPrimaryField()).';';
		echo($query.'<br>');
		$sth = $this->prepare($query);
		$sth->execute();
		unset($this);
	}
}