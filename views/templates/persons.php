<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Persons</title>
		<link rel="stylesheet" type="text/css" href="assets/css/persons.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="assets/js/persons.js"></script>
    </head>
    <body>
        <h1>Viewing persons</h1>
		<a href="?action=home">Back to home</a>
		<div class="table-content">
			<form action="?action=saveUsers" method="POST" id="usersForm">
				<input name="users" id="users" hidden>
				<table id="persons">
				<thead>
					<tr>
						<?php 
						if (isset($isAdmin))
							echo('<th>id</th>');
						?>
						<th>Firstname</th>
						<th>Lastname</th>
					</tr>
				</thead>
				<tbody>
				<?php
					$isUser = isset($users);
					foreach($persons as $p)
					{
						echo('<tr>');
						if ($isUser) 
						{
							if (isset($isAdmin))
								echo('<td><input name="id" value="'.$p['id'].'"/></td>');
							echo('<td><input name="fname" value="'.$p['fname'].'"/></td>');
							echo('<td><input name="sname" value="'.$p['sname'].'"/></td>');
						}
						else
						{
							echo('<td>'.$p['fname'].'</td>');
							echo('<td>'.$p['sname'].'</td>');
						}
						echo('</tr>');
					}
				?>
				</tbody>
				</table>
				<?php
				
				if ($isUser)
				{
					echo('<table id="users">
						<thead>
						<tr>
						<th>Username</th>
						<th>Description</th>');
					if (isset($isAdmin))
						echo('<th>pri</th><th>pwd</th>');
					echo('</tr>
						</thead>
						<tbody>');
					$i = 0;
					foreach($users as $u)
					{
						echo('<tr>');
						if (isset($isAdmin)) 
						{
							echo('<td><input name="user" value="'.$u['user'].'" disabled/></td>');
							echo('<td><input name="description" value="'.$u['description'].'"/></td>');
							echo('<td><input name="pri" value="'.$u['pri'].'"/></td>');
							echo('<td><input type="password" name="pwd"/></td>');
							echo('<td><button class="rm-btn" type="button">Remove</button></td>');
						}
						else 
						{
							echo('<td>'.$u['user'].'</td>');
							echo('<td>'.$u['description'].'</td>');
						}
						echo('</tr>');
						$i++;
					}
					echo('</tbody></table>');
				}
				?>
				<input type="submit" class="btn" value="Save"/>
			</form>
			<button class="btn" id="newUser">New</button>
		</div>
		<p id="response"></p>
		<form id="formRmUser" action="?action=removeUser" method="POST" hidden>
			<input id="rmUserInput" name="user" hidden>
		</form>
    </body>
</html>
