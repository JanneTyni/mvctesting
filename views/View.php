<?php
/**
 * View class creates view using model and template.
 *
 * @author Janne Tyni
 */
 
class View
{
	private $viewData = array();
    
    public function __construct()
    {
		
    }
    
	public function with($name, $value) 
	{
		$this->viewData[$name] = $value;
		return $this;
	}
	
    public function view($template)
    {
		foreach($this->viewData as $k => $v) {
			$$k = $v;
		}
        require_once('views/templates/'.$template.'.php');
		$this->viewData = array();
		return $this;
    }
}
