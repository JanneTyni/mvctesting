<?php
ini_set('display_errors', 1);
error_reporting(E_ALL|E_STRICT);

require_once('libs/Session.php');
require_once('models/User.php');
require_once('controllers/MainController.php');
require_once('views/View.php');
Session::initialize();
//print_r($_SESSION);
$view = new View();
$controller = new MainController($view);
$action = (isset($_GET['action']) ? $_GET['action'] : 'welcome');
$controller->{$action}();