$( document ).ready(function() {
	
	

	function collectTableData() 
	{
		var $persons = $('#persons tbody tr');
		var $users = $('#users tbody tr');
		var inputData = [];
		var usersLength = $users.length;
		
		$persons.each(function(index) {
			var person = {};
			var $this = $(this);
			
			function collectInputData() {
				var $this = $(this);
				var val = $this.val();
				if (val)
					person[$this.attr('name')] = val;
			}
			
			$this.find('td input').each(collectInputData);
			if (usersLength > 0 && $users[index])
				$($users[index]).find('td input').each(collectInputData);
			
			// TODO: check if person is empty
			if (!$.isEmptyObject(person)) inputData.push(person);
		});
		
		console.log(inputData);
		return inputData;
	}
	
	$('#usersForm').submit(function() {
		$('#users').val(JSON.stringify(collectTableData()));
		$('#usersForm :input').each(function() {
			if ($(this).attr('name') == 'users') return;
			$(this).remove();
		});
	});
	
	$('#newUser').click(function() {
		var $persons = $('#persons tbody');
		var $users = $('#users tbody');
		
		var $person = $persons.find('tr').first().clone();
		
		function clearInputs() {
			$(this).attr('value', '');
			$(this).prop('disabled', false);
		}
		
		$person.find('input').each(clearInputs);
		$persons.append($person);
		
		var $user = $users.find('tr').first().clone();
		$user.find('input').each(clearInputs);
		$users.append($user);
	});
	
	function removeUser(i) {
		var $u = $($('#users tbody tr')[i]);
		var user;

		$u.find('td input').each(function() {
			if ($(this).attr('name') == 'user')
				user = $(this).val();
		});
		
		$('#rmUserInput').val(user);
		console.log($('#rmUserInput').val());
		$("#formRmUser").submit();
	}
	
	$('.rm-btn').each(function(index) {
		$(this).click(function() { removeUser(index); });
	});

	test = collectTableData;
});